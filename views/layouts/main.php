<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="<?=Yii::$app->controller->id?>-<?=Yii::$app->controller->action->id?>">

<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    if(Yii::$app->controller->id==="site" && Yii::$app->controller->action->id==="index") {
        NavBar::begin([
            'brandLabel' => Html::img('@web/images/header.png', ['alt' => Yii::$app->name]),
            'brandUrl' => "#",
        ]);
        echo Nav::widget([
            'options' => [
                'class' => 'navbar-nav',
            ],
        ]);
        NavBar::end();
        echo "<div class='aboutbtn'>".Html::a('About','#')."</div>";
    }else if(Yii::$app->controller->id==="site" && Yii::$app->controller->action->id==="about"){
        echo "<div class='homebtn'>".Html::a('Home','#')."</div>";
    }else{
        NavBar::begin([
            'brandLabel' => "webChef",
            'brandUrl' => Yii::$app->homeUrl,
        ]);
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav'],
            'items' => [
                ['label' => 'About', 'url' => ['/pages/update?id=1']],
                ['label' => 'Posts', 'url' => ['/posts/index']],
                ['label' => 'Pages', 'url' => ['/pages/index']],
                ['label' => Yii::$app->user->isGuest?'Login':'Logout', 'url'=>Yii::$app->user->isGuest?['/site/login']:['/site/logout'], 'linkOptions' => ['data-method' => 'post']],
            ],
        ]);
        NavBar::end();
    }

    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <img src="/images/webChef_logoInverse.png">
         <div class="date"> <?= date('Y') ?></div>
    </div>
</footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
