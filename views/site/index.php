<?php

/* @var $this yii\web\View */

$this->title = 'webChef';
$data = $dataProvider->getModels();
foreach($data as $post):
    ?>

    <div class="blog-post">
        <img src="<?=$post->image?>">
        <h2 class="blog-post-title"><?=$post->title?></h2>
        <p class="blog-post-text"><?=$post->text?></p>
    </div>
<?php endforeach; ?>