<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$model = $dataProvider->getModels();
?>
<div class="about">
  <?php
  foreach($model as $page):
  if($page->id==1){?>
      <h1><?=$page->title?></h1>
      <p><?=$page->content?></p>
 <?php }
  endforeach;
  ?>
</div>
