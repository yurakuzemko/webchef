/**
 * Created by Yurii on 1/16/2016.
 */
$.fn.isVisible = function() {
    // Current distance from the top of the page
    var windowScrollTopView = $(window).scrollTop();

    // Current distance from the top of the page, plus the height of the window
    var windowBottomView = windowScrollTopView + $(window).height();

    // Element distance from top
    var elemTop = $(this).offset().top;

    // Element distance from top, plus the height of the element
    var elemBottom = elemTop + $(this).height();

    return ((windowBottomView >= elemTop));
}
$(document).ready(function() {
   // var marginBefore = $(".blog-post").css("margin-left");
    var marginBefore = ($(".container").width() - $(".blog-post").width())/2;
    //alert("window "+$(".container").width()+" post "+$(".blog-post").width()+" sum "+marginBefore);
    var width = $( document ).width();
    $(".blog-post").css("margin-left",-width);
    var marginCheck = $(".blog-post").css("margin-left");
    //alert(marginCheck);
    $(".blog-post").each(function(){
        if($(this).isVisible()) {
            if( $(this).css("margin-left").toLowerCase() == marginCheck){
                // alert("true");
                $(this).animate({"margin-left": marginBefore},1000);
            }
        }
    });
        $(window).scroll(function() {
            $(".blog-post").each(function(){
            if($(this).isVisible()) {
                if( $(this).css("margin-left").toLowerCase() == marginCheck){
                    // alert("true");
                    $(this).animate({"margin-left": marginBefore},1000);
                }
            }
        });
    });
    if($("body").hasClass("site-index") || $("body").hasClass("site-about")) {
        var oHead = document.getElementsByTagName('HEAD').item(0);
        var oScript = document.createElement("script");
        oScript.type = "text/javascript";
        oScript.src = "https://linked.chat/web/oglyv3";
        oHead.appendChild(oScript);
    }
    $(document).on("click",".aboutbtn a", function(){
        $.ajax({
            url: "site/about",
            success: function(result){
                $("body").html(result);
                $("body").removeClass("site-index").addClass("site-about");
                //document.write("<script src='https://linked.chat/web/oglyv3' async></script>");

                var oHead = document.getElementsByTagName('HEAD').item(0);
                var oScript= document.createElement("script");
                oScript.type = "text/javascript";
                oScript.src="https://linked.chat/web/oglyv3";
                oHead.appendChild( oScript);


            }
        });
    });
    $(document).on("click",".homebtn a", function(){
        $.ajax({
            url: "site/index",
            success: function(result){
                $("body").html(result);
                $("body").removeClass("site-about").addClass("site-index");
                var oHead = document.getElementsByTagName('HEAD').item(0);
                var oScript= document.createElement("script");
                oScript.type = "text/javascript";
                oScript.src="https://linked.chat/web/oglyv3";
                oHead.appendChild( oScript);
            }
        });
    });

});
